import java.lang.reflect.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

/**
 * Created by zlintz on 3/28/15.
 */
final public class ReflectionTester {

	private static final Logger LOGGER = Logger.getLogger(ReflectionTester.class.getName());

	final public void testClass(Class clazz) {
		this.performTestingOfGettersAndSetters(clazz);
	}

	/**
	 * This method allows us to avoid external library dependencies for things like StringUtils
	 * @param methodName
	 * @return
	 */
	private boolean startsWithGet(String methodName){
		if(methodName != null && methodName.length() > 3){
			return methodName.substring(0, 3).equals("get");
		}
		return false;
	}

	private void performTestingOfGettersAndSetters(Class clazz) {
		try {

			final Object objectInstance = clazz.newInstance();
			final Method[] methods = clazz.getDeclaredMethods();

			for (Method method : methods) {
				if (this.startsWithGet(method.getName()) && method.getParameterTypes().length == 0) {
					//We only care to test generic getters and setters
					final String setterMethodName = "s" + method.getName().substring(1);
					try {
						final Class returnTypeClass = method.getReturnType();
						final Method setterMethod = clazz.getMethod(setterMethodName, returnTypeClass);
						if (setterMethod != null) {
							//If we are here we have a getter with a corresponding setter
							final Object returnTypeInstance = getAnInstance(returnTypeClass);
							setterMethod.invoke(objectInstance, returnTypeClass.cast(returnTypeInstance));
							assertEquals(returnTypeClass.cast(returnTypeInstance), method.invoke(objectInstance));
						}
					} catch (NoSuchMethodException e) {
						//No corresponding setter method relative to getter. Continue.
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						LOGGER.log(Level.FINE, String.format("Unable to evaluate methods for %s() and %s() likely due to access modifiers", method.getName(), setterMethodName));
						e.printStackTrace();
					}
				}
			}

		} catch (Exception e) {
			this.exceptionMessageHandler(clazz, e);
		}
	}

	private void exceptionMessageHandler(Class clazz, Exception exception) {
		if (exception instanceof InstantiationException) {
			exception.printStackTrace();
			String reason = "Undetermined";
			if (clazz.isInterface()) {
				reason = clazz.getCanonicalName() + "is an interface. Cannot Instansiate.";
			} else if (Modifier.isAbstract(clazz.getModifiers())) {
				reason = clazz.getCanonicalName() + "is abstract. Cannot Instansiate.";
			}
			fail(String.format("Could not instansiate an instance of class: %s\nReason for Instatiantion Exception: %s", clazz.getCanonicalName(), reason));

		} else if (exception instanceof IllegalAccessException) {
			exception.printStackTrace();
			fail("Could not access a default public constructor.");
		} else {
			throw new RuntimeException("Unexpected Exception occurred while evaluating class:" + clazz.getCanonicalName(), exception);
		}

	}

	/**
	 * Returns the default value for the given Primitive Class.
	 *
	 * @param clazz
	 * @return
	 * @throws IllegalArgumentException if the provided class is not a primitive.
	 */
	private Object getPrimitiveValue(Class clazz) throws IllegalArgumentException {
		if (clazz.isPrimitive()) {
			//we are dealing with a primitive, return the relative default value
			switch (clazz.getName()) {
				case "byte":
				case "short":
				case "int":
					return 0;
				case "long":
					return 0L;
				case "float":
					return 0.0f;
				case "double":
					return 0.0d;
				case "char":
					return '\u0000';
				case "boolean":
					return false;
				default:
					throw new RuntimeException("Unknown Primitive");
			}
		}
		throw new IllegalArgumentException(String.format("Class \"%s\" is not a primitive", clazz.getName()));
	}

	private Object getAnInstance(Class clazz) {
		if (clazz.isPrimitive()) {
			//we are dealing with a primitive, return the relative default value
			return this.getPrimitiveValue(clazz);
		}

		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			if (clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
				e.printStackTrace();
				fail("Cannot instantiate a abstract class or interface.");
			}
			//No default public constructor, continue and try and make an instance based on another available constructor
		}

		final Constructor[] constructors = clazz.getConstructors();

		for (Constructor constructor : constructors) {

			if (Modifier.isPublic(constructor.getModifiers())) {

				final Class[] parameterTypes = constructor.getParameterTypes();
				final Object[] params = new Object[parameterTypes.length];

				for (int i = 0; i < parameterTypes.length; i++) {
					params[i] = getAnInstance(parameterTypes[i]);
				}

				try {
					return constructor.newInstance(params);
				} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}

		}

		throw new RuntimeException("Could not get an instance of class: " + clazz.getCanonicalName());
	}
}
