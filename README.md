# Why test a POJO?

Great question! I don't have a great answer.  :smile:

* One reason this may be helpful for you, is if you have a __Lines Covered code coverage requirement__ for your code to build. (Think maven plugin like jacoco with percentage covered requirement that fails the build if your lines covered are under a threshold).  
 * Pojo's unfortunately will count against this type of code coverage, which makes it particularly difficult to create a new project.
 * This may not be an issue if you trust everyone you are working with to put POJO's in a single package to exclude, but engineers are not always consistent.
  * Speaking of _trust_ do you _trust_ the engineer not to override :scream: your coverage requirment that is in the parent pom???
* You are a little bit OCD and the thought of :heart_eyes:__100% coverage really excites you__:clap: